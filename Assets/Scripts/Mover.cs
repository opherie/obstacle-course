using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Mover : MonoBehaviour
{
    [SerializeField] float playerSpeed = 0.1f;

    void Start()
    {
        PrintInstruction();
    }

    void Update()
    {
        MovePlayer();
    }

    void PrintInstruction()
    {
        Debug.Log("Welcome to the game");
        Debug.Log("Move your player with arrow keys");
        Debug.Log("Don't hit the walls!");
    }

    void MovePlayer()
    {
        float xValue = Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime;
        float zValue = Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime;

        transform.Translate(xValue, 0f, zValue);
    }
}
