using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{
    [SerializeField] float timeToWaitUntilDrop = 3f;

    Rigidbody myRigidbody;
    MeshRenderer myRenderer;
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myRenderer = GetComponent<MeshRenderer>();

        ViewAndDropTheCube(false);
    }

    void Update()
    {
        if(Time.time >= timeToWaitUntilDrop)
        {
            ViewAndDropTheCube(true);
        }
    }

    void ViewAndDropTheCube(bool value)
    {
        myRenderer.enabled = value;
        myRigidbody.useGravity = value;
    }
}
